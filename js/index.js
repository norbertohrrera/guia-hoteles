    	$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$('.carousel').carousel({
    			interval: 1000
    		})	

    		$('#contactar').on('show.bs.modal', function (e){
    			console.log('el modal contacto se esta mostrando');

    			$('#contactarBtn').removeClass('btn btn-outline-success');
    			$('#contactarBtn').addClass('btn btn-primary');
    			$('#contactarBtn').prop('disabled',true);
    		});
    		$('#contactar').on('shown.bs.modal', function (e){
    			console.log('el modal contacto se mostró');
    		});
    		$('#contactar').on('hide.bs.modal', function (e){
    			console.log('el modal contacto se va a ocultar');
    		});
    		$('#contactar').on('hidden.bs.modal', function (e){
    			console.log('el modal contacto se ocultó');
    			$('#contactarBtn').removeClass('btn btn-primary');
    			$('#contactarBtn').addClass('btn btn-outline-success');
    			$('#contactarBtn').prop('disabled',false);
    		});

    	});
